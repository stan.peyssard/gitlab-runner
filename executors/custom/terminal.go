//go:build !windows
// +build !windows

package custom

import (
	"errors"

	terminalsession "gitlab.com/stan.peyssard/gitlab-runner/session/terminal"
)

func (e *executor) Connect() (terminalsession.Conn, error) {
	return nil, errors.New("not yet supported")
}
