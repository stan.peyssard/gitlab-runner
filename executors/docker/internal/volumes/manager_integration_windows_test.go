//go:build integration
// +build integration

package volumes_test

import (
	"testing"

	"gitlab.com/stan.peyssard/gitlab-runner/executors/docker/internal/volumes/parser"
)

var testCreateVolumesLabelsDestinationPath = `C:\test`

func TestCreateVolumesLabels(t *testing.T) {
	testCreateVolumesLabels(t, parser.NewWindowsParser())
}
